CREATE TABLE account (
    id SERIAL,
    email VARCHAR(128) UNIQUE,
    create_at DATE NOT NULL DEFAULT NOW(),
    update_at DATE NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id)
);

CREATE TABLE post (
    id SERIAL,
    title VARCHAR(128),
    content VARCHAR(1024), -- Will extend with ALTER
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
    create_at TIMESTAMPZ NOT NULL DEFAULT NOW(),
    update_at TIMESTAMPZ NOT NULL DEFAULT NOW()
);

CREATE TABLE comment (
    id SERIAL,
    content TEXT NOT NULL,
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
    post_id INTEGER REFERENCES post(id) ON DELETE CASCADE,
    create_at TIMESTAMPZ NOT NULL DEFAULT NOW(),
    update_at TIMESTAMPZ NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id)
);

CREATE TABLE fav (
    id SERIAL,
    opps TEXT, -- Will remove latter with ALTER
    post_id INTEGER REFERENCES post(id) ON DELETE CASCADE,
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
    create_at TIMESTAMPZ NOT NULL DEFAULT NOW(),
    update_at TIMESTAMPZ NOT NULL DEFAULT NOW(),
    UNIQUE(post_id, account_id),
    PRIMARY KEY(id)
);

ALTER TABLE fav DROP COLUMN oops; -- delete the column opps on table fav
ALTER TABLE post ALTER COLUMN content TYPE TEXT; -- change the type of content on table post to TEXT
ALTER TABLE fav ADD COLUMN howmuch INTEGER; -- add new column called howmuch with INTEGER type 

-- To cast some DATE
-- SELECT NOW()DATE, CAST(NOW() AS DATE), CAST(NOW() AS TIME);

-- for intevals
-- SELECT NOW()M NOW() - INTERVAL '2 days', (NOW - INTERVAL '2 DAYS')::DATE;

CREATE TABLE bigtext(
    content TEXT
);

INSERT INTO bigtext(content) SELECT repeat('This is record number ', 1) || generate_series(100000, 199999) || repeat('of quite a few text records.', 1);

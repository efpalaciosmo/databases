CREATE TABLE make (
    id SERIAL,
    name VARCHAR(128) UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE model (
  id SERIAL,
  name VARCHAR(128),
  make_id INTEGER REFERENCES make(id) ON DELETE CASCADE,
  PRIMARY KEY(id)
);

INSERT INTO make (name) VALUES ('GMC');
INSERT INTO make (name) VALUES ('Mercedes-Benz');
INSERT INTO model (name, make_id) VALUES ('K15 Jimmy 4WD', 1);
INSERT INTO model (name, make_id) VALUES ('K15 Pickup 4WD', 1);
INSERT INTO model (name, make_id) VALUES ('K15 Suburban 4WD', 1);
INSERT INTO model (name, make_id) VALUES ('Metris (Passenger Van)', 2);
INSERT INTO model (name, make_id) VALUES ('R320 Bluetec', 2);
